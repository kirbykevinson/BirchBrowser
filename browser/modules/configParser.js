//Переменные
	var searchEngine = '';
	var homePage = '';
	var navBarPos = '';
	var configStrings = [
	'searchEngine',
	'homePage',
	'navBarPos'
	];
//--Переменные

//Автор BirchBrowser и используемого здесь кода - Lupshenko

//Настройки (Операции)
	function loadSettings() {
		var config;
		try {
			config = fs.readFileSync(confPath + 'config.ini', 'utf-8');
		} catch(a) {
			config = '';
		}
		document.getElementById('settingsContainer').innerHTML = config;
	}

	function writeSettings() {
		var toWrite = '';
		for (var i = 0; i < configStrings.length; i++) {
			toWrite += configStrings[i] + ' = \'' + eval(configStrings[i]) + '\'; \n';
		}
		fs.writeFileSync(confPath + 'config.ini', toWrite);
	}
//--Настройки (Операции)

//Настройки

	function resetSettings() {
			fs.writeFileSync(confPath + 'bookmarks.ini', '');
			fs.writeFileSync(confPath + 'config.ini', '');
			fs.writeFileSync(confPath + 'plugins.ini', '');
			fs.writeFileSync(confPath + 'history.ini', '');
			alert('Для применения сброса перезагрузите браузер!');
	}


	function exportSettings() {
		var txt1, txt2, txt3, txt4;
		try {
		txt1 = fs.readFileSync(confPath + 'bookmarks.ini', 'utf-8');
		} catch(a) {txt2 = '';}
		try {
		txt2 = fs.readFileSync(confPath + 'config.ini', 'utf-8');
		} catch(a) {txt3 = '';}
		try {
		txt3 = fs.readFileSync(confPath + 'plugins.ini', 'utf-8');
		} catch(a) {txt4 = '';}
		try {
		txt4 = fs.readFileSync(confPath + 'history.ini', 'utf-8');
		} catch(a) {txt5 = '';}
		var path = prompt('Путь до папки, куда нужно произвести экспорт настроек', '');
		if (path) {
			fs.writeFileSync(path + 'bookmarks.ini', txt1);
			fs.writeFileSync(path + 'config.ini', txt2);
			fs.writeFileSync(path + 'plugins.ini', txt3);
			fs.writeFileSync(path + 'history.ini', txt4);
			alert('Экспорт произведен!');
		} else {
			alert('Ошибка! Путь не введен!');
		}
	}

	function importSettings() {
		var path = prompt('Путь до папки, откуда нужно произвести импорт настроек', '');
		var check = false;
		var txt1, txt2, txt3, txt4;
		try {
		txt1 = fs.readFileSync(path + 'bookmarks.ini', 'utf-8');
		} catch(a) {txt1 = '';}
		try {
		txt2 = fs.readFileSync(path + 'config.ini', 'utf-8');
		} catch(a) {txt2 = '';}
		try {
		txt3 = fs.readFileSync(path + 'plugins.ini', 'utf-8');
		} catch(a) {txt3 = '';}
		try {
		txt4 = fs.readFileSync(path + 'history.ini', 'utf-8');
		} catch(a) {txt4 = '';}
		if (path) {		
			fs.writeFileSync(confPath + 'bookmarks.ini', txt1);
			fs.writeFileSync(confPath + 'config.ini', txt2);
			fs.writeFileSync(confPath + 'plugins.ini', txt3);
			fs.writeFileSync(confPath + 'history.ini', txt4);
			alert('Для применения настроек перезагрузите браузер!');
		} else {
			alert('Ошибка! Путь не введен!');
		}
	}
	
	function getSearch() {
		if (searchEngine == '') {
			searchEngine = 'http://duckduckgo.com/';
		}
		if (searchEngine[searchEngine.length-1] != '/') {
			searchEngine = searchEngine + '/';
		}
		if (searchEngine.indexOf('://')+1) { } else {
			searchEngine = 'http://' + searchEngine;
		}
		document.getElementById('searchStr').value = searchEngine;
		fs.writeFileSync('forStart/BIRCHsearch.html', searchEngine);
	}

	function setSearch() {
		searchEngine = document.getElementById('searchStr').value;
		getSearch();
		writeSettings();
	}

	function getHP(arg) {
		if (homePage != '') {
			if (!(homePage.indexOf('://') + 1) && homePage != 'pages/start.html') {
				homePage = 'http://' + homePage;
			} 
		} else {
			homePage = 'pages/start.html';
		}
		document.getElementById('HPStr').value = homePage;
		if (arg) {
			document.getElementById('webView').src = homePage;
		}
	}
	
	function setHP() {
		homePage = document.getElementById('HPStr').value;
		getHP();
		writeSettings();
	}
	
	function getNavBar() {
		switch(navBarPos) {
			case 'left' :
			document.getElementById('pluginsContainer').innerHTML += '<style>#navBarTrigger { left : 0; right : 98.5vw; } #navBar { left : 0; right : 90vw; } .tab { right : 0; left : 10vw; }</style>';
			break;
			
			case 'right' :
			document.getElementById('pluginsContainer').innerHTML += '<style>#navBarTrigger { right : 0; left : 98.5vw; } #navBar { right : 0; left : 90vw; } .tab { left : 0; right : 10vw; }</style>';
			break;
			
			default :
			document.getElementById('pluginsContainer').innerHTML += '<style>#navBarTrigger { left : 0; right : 98.5vw; } #navBar { left : 0; right : 90vw; } .tab { right : 0; left : 10vw; }</style>';
			break;
		}
	}
	
	function setNavBar(arg) {
		navBarPos = arg;
		getNavBar();
		writeSettings();
	}
//--Настройки
