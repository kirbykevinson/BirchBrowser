//Переменные
	var allPlugins = [];
	var loadedPlugins = [];
	var pluginsPath = '../plugins/';
//--Переменные

//Автор BirchBrowser и используемого здесь кода - Lupshenko

//Плагины
	function loadPlugins() {
			var pluginsContainer = document.getElementById('pluginsContainer'); 
			var pluginsTiles1 = document.getElementById('pluginsTiles1');
			var pluginsTiles2 = document.getElementById('pluginsTiles2');
		//Подгрузка списка всех плагинов
			try {
				allPlugins = fs.readdirSync(pluginsPath);
			} catch(a) {
				allPlugins = [];
			}
			for (var i = 0; i  < allPlugins.length; i++) {
				if (allPlugins[i] != '.gitkeep') {
					pluginsTiles1.innerHTML += '<a href="#" class=\"BGmenuItem dark transparent\" onclick=\"transportPlugin(\'add\', ' + i + ')\">' + allPlugins[i] + '</a>';
				}
			}
		//--Подгрузка списка всех плагинов
		
		//Подгрузка выбранных плагинов
			try {
				loadedPlugins = fs.readFileSync(confPath + 'plugins.ini', 'utf-8');
			} catch(a) {
				loadedPlugins = '';
			}
			if (loadedPlugins.indexOf('\n') + 1) {
				loadedPlugins = loadedPlugins.replace(/\n/mg, '');
				loadedPlugins = loadedPlugins.replace(/\r/mg, '');
			}
			//
			if (loadedPlugins) {
			loadedPlugins = loadedPlugins.split(', ');
			} else {
				loadedPlugins = [];
			}
			//
			for (var i = 0; i < loadedPlugins.length; i++) {
				pluginsTiles2.innerHTML += '<a href="#" class=\"BGmenuItem dark transparent\" onclick=\"transportPlugin(\'del\', ' + i + '); remove();\">' + loadedPlugins[i] + '</a>';
				var curPlgSrc = pluginsPath + loadedPlugins[i];
				var curPlgTxt;
				try {
					curPlgTxt = fs.readFileSync(curPlgSrc, 'utf-8')
				} catch(a) {
					curPlgTxt = '';
				}
				var curPlgCntr = document.createElement('script');
				curPlgCntr.innerHTML = curPlgTxt;
				curPlgCntr.type = 'text/javascript';
				pluginsContainer.appendChild(curPlgCntr);
			}
		//--Подгрузка выбранных плагинов
	}
	
	function transportPlugin(condition, arg) {
		if (condition == 'add') {
			loadedPlugins[loadedPlugins.length] = allPlugins[arg];
			pluginsTiles2.innerHTML += '<a href="#" class=\"BGmenuItem dark transparent\" onclick=\"transportPlugin(\'del\', ' + (loadedPlugins.length - 1) + '); remove();\">' + loadedPlugins[loadedPlugins.length - 1] + '</a>';
			var toWrite = loadedPlugins.join(', ');
			fs.writeFileSync(confPath + 'plugins.ini', toWrite);
		} else {
			loadedPlugins.splice(arg, 1);
			var toWrite = loadedPlugins.join(', ');
			fs.writeFileSync(confPath + 'plugins.ini', toWrite);
			pluginsTiles2.innerHTML = '';
			for (var i = 0; i < loadedPlugins.length; i++) {
					pluginsTiles2.innerHTML += '<a href="#" class=\"BGmenuItem dark transparent\" onclick=\"transportPlugin(\'del\', ' + i + '); remove();\">' + loadedPlugins[i] + '</a>';
			}
		}
	}
//--Плагины
