//Автор BirchBrowser и используемого здесь кода - Lupshenko

//История

	function unIncognito() {
		if (incognito == true) {
			incognito = false;
			alert('Запись истории включена');
		} else {
			incognito = true;
			alert('Запись истории отключена');
		}
	}

	function writeHistory() {
		if (incognito == false) {
			var tlt = document.getElementById('webView').contentWindow.document.title;
			var txt;
			try {
			txt = fs.readFileSync(confPath + 'history.ini', 'utf-8');
			} catch(a) {
				txt = '';
			}
			var current = document.getElementById('webView').contentWindow.location;
			var toWrite = '<a href=\"' + current + '\" target=\"brws\" class=\"BGmenuItem dark transparent\" onclick=\"clAll()\" oncontextmenu=\"removeHistoryItem(this)\">' + tlt + '</a>' + txt;
			fs.writeFileSync(confPath + 'history.ini', toWrite);
		}
		loadHistory();
	}

	function loadHistory() {
		var txt;
		try {
		txt = fs.readFileSync(confPath + 'history.ini', 'utf-8');
		} catch(a) {
			txt = '';
		}
		document.getElementById('historyHere').innerHTML = txt;
	}
	
	function removeHistoryItem(arg) {
		arg.remove();
		fs.writeFileSync(confPath + 'history.ini', document.getElementById('historyHere').innerHTML);
	}
	
	function resetHistory() {
		fs.writeFileSync(confPath + 'history.ini', '');
		loadHistory();
	}

//--История

//Закладки

	function loadBkms() {
		var txt;
		try {
		txt = fs.readFileSync(confPath + 'bookmarks.ini', 'utf-8');
		} catch(a) {
			txt = '';
		}
		document.getElementById('bkmsHere').innerHTML = txt;
	}

	function customBkm() {
		var siteAdress = prompt('Адрес сайта :', '');
		var siteName = prompt('Имя сайта :', '');
		
		if (siteAdress.indexOf('://')+1) {} else {siteAdress = 'http://' + siteAdress;}
		
		if (siteAdress && siteName) {
		var txt;
		try {
		txt = fs.readFileSync(confPath + 'bookmarks.ini', 'utf-8');
		} catch(a) {
			txt = '';
		}
		fs.writeFileSync(confPath + 'bookmarks.ini', txt + '<a class=\"BGmenuItem dark transparent\" href=\"' + siteAdress + '\" target=\"brws\" class=\"BGbutton dark transparent\" onclick=\"clAll()\" oncontextmenu=\"removeBkmItem(this)\">' + siteName + '</a>');
		} else {
			alert('Ошибка! Введено не все!');
		}
		loadBkms();
	}

	function addBkm() {
		var txt;
		try {
		txt = fs.readFileSync(confPath + 'bookmarks.ini', 'utf-8');
		} catch(a) {
			txt = '';
		}
		var loc = document.getElementById('webView').contentWindow.location;
		var tlt = document.getElementById('webView').contentWindow.document.title;
		fs.writeFileSync(confPath + 'bookmarks.ini', txt + '<a class=\"BGmenuItem dark transparent\" href=\"' + loc + '\" target=\"brws\" class=\"a\" onclick=\"clAll()\" oncontextmenu=\"removeBkmItem(this)\">' + tlt + '</a>');
		loadBkms();
	}

	function removeBkmItem(arg) {
		arg.remove();
		fs.writeFileSync(confPath + 'bookmarks.ini', document.getElementById('bkmsHere').innerHTML);
	}
	
	function resetBkms() {
		fs.writeFileSync(confPath + 'bookmarks.ini', '');
		loadBkms();
	}

//--Закладки
