//Переменные
	var gui = require('nw.gui');
	var fs = require('fs');
	var win = gui.Window.get();
	var incognito = false;
	var ctrlTrigger = false;
	var maximized = false;
	var confPath = '../config/';
	var curPageZoom = 1;
	var zoomIndTimeout;
//--Переменные

//Справочные переменные
	var versionsHelp = [
		'Вид версии - BirchBrowser [редакция] [вид релиза(номер)] [время сборки]',
		'',
		'Редакция:',
		'L - старая',
		'N - обычная',
		'SE - специальная',
		'',
		'Вид релиза:',
		'NV - нумерованый',
		'RR - rolling release',
		'PR - прототип',
		'',
		'Время сборки - Число.Месяц.Год'
	];
//--Справочные переменные

//Автор BirchBrowser и используемого здесь кода - Lupshenko

//Служебное

	function initAll() {
		win.unmaximize();
		loadSettings();
		getSearch();
		getHP(true);
		getNavBar();
		if ( !(gui.App.fullArgv.indexOf('--safe')+1) ) {
			loadPlugins();
		}
		loadBkms();
		loadHistory();
	}

	win.on('new-win-policy', function (frame, url, policy) {
		if (url != window.top.location) {
		policy.forceCurrent();
		}
	});
	
	function maximinimize() {
		if (maximized == false) {
			win.maximize();
			maximized = true;
		} else {
			win.unmaximize();
			maximized = false;
		}
	}
	
	function changeSrc() {
		document.getElementById('webView').contentWindow.document.getElementsByTagName('html')[0].innerHTML = document.getElementById('pageSrc').value;
	}
	
	function runCode() {
		var code = document.createElement('script');
		code.innerHTML = document.getElementById('jsCmd').value;
		document.getElementById('pluginsContainer').appendChild(code);
	}

//--Служебное

//Веб

	function browse() {
		var rawUrlValue = document.form1.url.value;
		if (rawUrlValue == 'about:blank' || rawUrlValue == 'ABOUT:BLANK') {
			document.getElementById('webView').src = 'pages/start.html';
		} else if (rawUrlValue.indexOf('://')+1) {
			document.getElementById('webView').contentWindow.location = rawUrlValue;
		} else {
			if (rawUrlValue.indexOf('.')+1 && !(rawUrlValue.indexOf(' ')+1) ) {
				document.getElementById('webView').contentWindow.location = 'http://' + rawUrlValue;
			} else {
				searchIt();
			}
		}
	}
	
	function refresh() {
		document.getElementById('webView').contentWindow.location.reload(true);
	}

	function updTitle() {
		if (document.getElementById('webView').contentWindow.location == 'data:text/html,chromewebdata' || document.getElementById('webView').contentWindow.location == 'about:blank') {
			document.getElementById('webView').src = 'pages/404.html';
		}
			document.form1.url.value = document.getElementById('webView').contentWindow.location; 
			document.getElementById('pageSrc').value = document.getElementById('webView').contentWindow.document.getElementsByTagName('html')[0].innerHTML;
			zoomPage();
	}
	
	function zoomPage() {
		document.getElementById('webView').contentWindow.document.body.style.zoom = curPageZoom;
		document.getElementById('zoomIndicator').innerHTML = curPageZoom;
	}

	function backIt() {
		document.getElementById('webView').contentWindow.history.back();
	}

	function forwardIt() {
		document.getElementById('webView').contentWindow.history.forward();
	}

	function searchIt() {
		var a = searchEngine + '?q=' + document.form1.url.value;
		document.getElementById('webView').contentWindow.location = a;
	}

//--Веб

//Отображение

	function clAll() {
		var tabs = document.getElementsByClassName('tab');
		for (var i = 0; i < tabs.length; i++) {
			tabs[i].style.display = 'none';
		}
		document.getElementById('navBar').style.display = 'none';
	}

	function clNavBar() {
		var tabs = document.getElementsByClassName('tab');
		var checked = true;
		for (var i = 0; i < tabs.length; i++) {
			if (tabs[i].style.display == 'none') {} else {
				checked = false;
				break;
			}
		}
		if (checked) {
			unhoverIt('navBar');
		}
	}

	function selectIt(arg, arg2, arg3) {
		var obj = document.getElementById(arg);
		var obj2 = document.getElementById(arg2);
		var obj3 = document.getElementById(arg3);
		var tabs = document.getElementsByClassName('tab');
		for (var i = 0; i < tabs.length; i++) {
			tabs[i].style.display = 'none';
		}
		obj.style.display = 'block';
		obj2.style.display = 'block';
		obj3.style.display = 'block';
	}

	function unhoverIt(arg) {
		var obj = document.getElementById(arg);
		obj.style.display = 'none';
	}

	function hoverIt(arg) {
		var obj = document.getElementById(arg);
		obj.style.display = 'block';
	}
	
	function touchNavBar() {
		if (document.getElementById('navBar').style.display == 'block') {
			clAll();
		} else {
			hoverIt('navBar');
		}
	}

//--Отображение

//Горячие клавиши

	function ctrlKeys() {
		var key = event.keyCode;
		if (key == 17) {
			ctrlTrigger = false;
			document.getElementById('zoomIndicator').style.display = 'none';
		}
	}

	function keys() {
		var key = event.keyCode;
		switch(key) {
			case 116:
				refresh();
				break;
				
			case 120:
				searchIt();
				break;
			
			case 17:
				ctrlTrigger = true;
				break;
		}
		
	//
		if (ctrlTrigger == true) {
			switch(key) {
				case 38:
					if (curPageZoom < 25) {
						curPageZoom += 0.25;
					}
					zoomPage();
					document.getElementById('zoomIndicator').style.display = 'block';
				break;
				
				case 40:
					if (curPageZoom > 0.25) {
						curPageZoom -= 0.25;
					}
					zoomPage();
					document.getElementById('zoomIndicator').style.display = 'block';
					break;
					
				case 66:
					touchNavBar();
					break;
				
				case 78:
					window.open('shell.html');
					break;
				
				case 36:
					document.getElementById('webView').src = homePage;
					break;
				
				case 37:
					backIt();
					break;
				
				case 39:
					forwardIt();
					break;
				
				case 79:
					var fileName = prompt('Введите полный путь до файла для открытия', '');
					if (fileName) {
						document.getElementById('webView').contentWindow.location = 'file:///' + fileName;
					} else {
						alert('Ошибка! Путь не введен!');
					}
					break;
			}
		}
	//
	}

//--Горячие клавиши
